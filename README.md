## Keterangan

Virtual Internship Experience (Investree) - Fullstack - Sulthan Rafif

## Instalasi Aplikasi

-   Unduh aplikasi ini
-   Install xampp dan composer
-   Copy aplikasi ini ke `xampp/htdocs/`
-   Copy file `.env.example` dan ubah nama file menjadi `.env`
-   Ubah konfigurasi yang ada di file .env seperti database dan nama aplikasi anda
-   Buka terminal dan masuk ke folder aplikasi kalian
-   Run `$ composer install`
-   Run `$ php artisan key:generate`
-   Run `$ php artisan storage:link`
-   Run `$ php artisan passport:install`
-   Run `$ php artisan optimize`
-   Run `$ php artisan migrate --seed`
-   Run `$ npm install`
-   Run `$ npm run dev`
-   Run `$ php artisan serve untuk akses aplikasi`
-   Buka browser dan ketik url ini: `http://localhost:8000`

## Pengujian di POSTMAN

-   Link API untuk Category: https://www.postman.com/collections/c1756e17c3267fc52d88
-   Link API untuk Auth: https://www.postman.com/collections/847018912e6516be8d99
-   Link API untuk Article: https://www.postman.com/collections/097b376e62323763c490

## Menjalankan Unit Testing

-   Perintah menjalankan testing untuk API Category:
    `$ php vendor/phpunit/phpunit/phpunit tests/Feature/Http/Controllers/Api/Category/CategoryControllerTest.php`
-   Perintah menjalankan testing untuk API Article:
    `$ php vendor/phpunit/phpunit/phpunit tests/Feature/Http/Controllers/Api/Article/ArticleControllerTest.php`
