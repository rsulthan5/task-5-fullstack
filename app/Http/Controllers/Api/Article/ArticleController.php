<?php

namespace App\Http\Controllers\Api\Article;

use App\Http\Controllers\Controller;
use App\Http\Requests\Article\ArticleRequest;
use App\Http\Resources\Article\ArticleIndexResource;
use App\Http\Resources\Article\ArticleShowResource;
use App\Models\Article;
use Illuminate\Support\Facades\Storage;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = Article::paginate(5);

        return ArticleIndexResource::collection($articles);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ArticleRequest $request)
    {
        $uploaded_image_response = $this->uploadImage($request);

        $article = auth()
            ->user()
            ->articles()
            ->create($this->articleStore($request));

        if (!$article) {
            return response()->json([
                'success' => false,
                'message' => 'Article not added'
            ]);
        }

        return response()->json([
            'success' => true,
            'data' => $article->toArray(),
            'image_response' => $uploaded_image_response
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Article $article)
    {
        return new ArticleShowResource($article);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ArticleRequest $request, $id)
    {
        $article = auth()->user()->articles()->find($id);

        if (!$article) {
            return response()->json([
                'success' => false,
                'message' => 'Article not found'
            ], 400);
        }

        $uploaded_image_response = $this->uploadImage($request);

        $updated = $article->update($this->articleStore($request));

        if (!$updated) {
            return response()->json([
                'success' => false,
                'message' => 'Article can not be updated'
            ]);
        }

        return response()->json([
            'success' => true,
            'data' => $article->toArray(),
            'image_response' => $uploaded_image_response
        ], 400);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $article = auth()->user()->articles()->find($id);

        if (!$article) {
            return response()->json([
                'success' => false,
                'message' => 'Article not found'
            ], 400);
        }

        if ($article->delete()) {
            return response()->json([
                'success' => true,
                'message' => 'Article successfully deleted'
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Article can not be deleted'
            ], 500);
        }
    }

    public function articleStore(ArticleRequest $request)
    {
        return [
            'title' => $request->title,
            'content' => $request->content,
            'image' => $request->image->getClientOriginalName(),
            'category_id' => $request->category,
        ];
    }

    public function uploadImage(ArticleRequest $request)
    {
        /**
         * Proses upload file gambar
         *
         * Nama folder tempat file
         * disimpan
         */
        $upload_folder = 'users';

        /**
         * Mengambil file image
         */
        $image = $request->file('image');

        /**
         * Proses memasukkan file gambar yang sudah diupload
         * ke destinasi folder yang sudah ditentukan.
         * Serta menentukan tipe disk public agar dapat
         * diakses secara public.
         */
        $image_uploaded_path = $image->store($upload_folder, 'public');

        /**
         * Upload image response
         */
        $uploaded_image_response = array(
            "image_name" => basename($image_uploaded_path),
            "image_url" => Storage::disk('public')->url($image_uploaded_path),
            "mime" => $image->getClientMimeType()
        );

        return $uploaded_image_response;
    }
}
