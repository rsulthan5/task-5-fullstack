<?php

namespace App\Http\Controllers\Web\Article;

use App\Http\Controllers\Controller;
use App\Http\Requests\Article\ArticleRequest;
use App\Http\Resources\Article\ArticleIndexResource;
use App\Models\Article;
use App\Models\Category;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = ArticleIndexResource::collection(Article::paginate(5));
        return view('article.index', compact('articles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('article.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ArticleRequest $request)
    {
        $this->uploadImage($request);

        auth()
            ->user()
            ->articles()
            ->create($this->articleStore($request));

        return redirect()->route('article.index')->with('success', 'Data artikel berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $article = Article::find($id);

        if (!$article) {
            return redirect()->route('article.index')->with('failed', 'Data Article tidak ditemukan');
        }

        return view('article.detail', compact('article'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $article = auth()->user()->articles()->find($id);

        $categories = Category::all();

        if (!$article) {
            return redirect()->route('article.index')->with('failed', 'Data Category tidak ditemukan');
        }

        return view('article.edit', compact('article', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ArticleRequest $request, $id)
    {
        $this->uploadImage($request);

        auth()
            ->user()
            ->articles()
            ->find($id)
            ->update($this->articleStore($request));

        return redirect()->route('article.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $article = auth()
            ->user()
            ->articles()
            ->find($id);

        if (!$article) {
            return redirect()->route('article.index')->with('failed', 'Data Article tidak dapat dihapus. Data tidak dapat ditemukan');
        }

        $article->delete();
        return redirect()->route('article.index');
    }

    public function articleStore(ArticleRequest $request)
    {
        return [
            'title' => $request->title,
            'content' => $request->content,
            'image' => $request->image->hashName(),
            'category_id' => $request->category,
        ];
    }

    public function uploadImage(ArticleRequest $request)
    {
        $upload_folder = 'users';

        $request
            ->file('image')
            ->store($upload_folder, 'public');
    }
}
