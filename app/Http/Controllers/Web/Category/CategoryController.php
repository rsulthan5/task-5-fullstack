<?php

namespace App\Http\Controllers\Web\Category;

use App\Http\Controllers\Controller;
use App\Http\Requests\Category\CategoryRequest;
use App\Http\Resources\Category\CategoryIndexResource;
use App\Models\Category;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = CategoryIndexResource::collection(Category::paginate(5));
        return view('category.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $request)
    {
        auth()
            ->user()
            ->categories()
            ->create($this->categoryStore($request));

        return redirect()->route('category.index')->with('success', 'Data Category berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = Category::find($id);

        if (!$category) {
            return redirect()->route('category.index')->with('failed', 'Data Category tidak ditemukan');
        }

        return view('category.detail', compact('category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = auth()->user()->categories()->find($id);

        if (!$category) {
            return redirect()->route('category.index')->with('failed', 'Data Category tidak ditemukan');
        }

        return view('category.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryRequest $request, $id)
    {
        auth()
            ->user()
            ->categories()
            ->find($id)
            ->update($this->categoryStore($request));

        return redirect()->route('category.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = auth()
            ->user()
            ->categories()
            ->find($id);

        if (!$category) {
            return redirect()->route('category.index')->with('failed', 'Data Category tidak dapat dihapus. Data tidak dapat ditemukan');
        }

        $category->delete();
        return redirect()->route('category.index');
    }

    public function categoryStore(CategoryRequest $request)
    {
        return [
            'name' => $request->name,
        ];
    }
}
