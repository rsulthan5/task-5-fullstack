<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    protected $fillable = [
        'name'
    ];

    protected $guarded = ['id'];

    protected $with = ['articles', 'user'];

    public function articles()
    {
        return $this->hasMany(Article::class, 'category_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
