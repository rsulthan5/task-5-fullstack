@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 mb-3">
            <h4>Form Create Article</h4>
        </div>
        <div class="col-md-6">
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <form action="{{ route('article.store') }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form-group mb-3">
                    <label>Title Name:</label>
                    <input type="text" name="title" class="form-control" />
                </div>
                <div class="form-group mb-3">
                    <label>Category:</label>
                    <select name="category" class="form-control">
                        @forelse($categories as $item)
                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                        @empty
                        @endforelse
                    </select>
                </div>
                <div class="form-group mb-3">
                    <label>Content</label>
                    <textarea class="form-control" name="content" rows="4"></textarea>
                </div>
                <div class="form-group mb-3">
                    <label>Gambar</label>
                    <input class="form-control" type="file" name="image" />
                </div>
                <div class="form-group">
                    <button class="btn btn-md btn-primary" type="submit">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
