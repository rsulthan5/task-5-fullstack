@extends('layouts.app')
@section('content')
<div class="container mt-4">
    <div class="row">
        <div class="col-lg-8">
            <article>
                <header class="mb-2">
                    <h1 class="fw-bolder mb-1">
                        {{ $article->title }}
                    </h1>
                    <div class="text-muted fst-italic mb-2">
                        Posted on {{\Carbon\Carbon::parse($article->created_at)->toFormattedDateString()}} by
                        {{$article->user->name}}
                    </div>
                    <a href="" class="badge bg-secondary text-decoration-none link-light">
                        {{ $article->category->name }}
                    </a>
                </header>
                <a href="{{ route('article.index') }}" class="badge bg-primary text-decoration-none link-light mb-3">
                    <span data-feather="arrow-left">
                        Back to all articles
                    </span>
                </a>
                @if($article->user == auth()->user())
                <a href="{{ route('article.edit', $article->id) }}"
                    class="badge bg-warning text-decoration-none link-light mb-3">
                    <span data-feather="edit">
                        Edit
                    </span>
                </a>
                <a href="{{ route('article.destroy', $article->id) }}"
                    class="badge bg-primary text-decoration-none link-light mb-3">
                    <span data-feather="x-circle">
                        Delete
                    </span>
                </a>
                @endif
                <figure class="mb-4">
                    <img width="500" src="{{ asset('storage/users/'.$article->image) }}" class="img-fluid rounded"
                        alt="image">
                </figure>
                <section class="mb-5">
                    <p class="fs-5 mb-4">
                        {{ $article->content }}
                    </p>
                </section>
            </article>
        </div>
    </div>
</div>
@endsection
