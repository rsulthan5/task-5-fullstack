@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 md-3 mb-3">
            <h4>Detail Category of {{ $category->name }}</h4>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <h2>{{ $category->name }}</h2>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <h4>Created at:</h4>
                            <p>{{ \Carbon\Carbon::parse($category->created_at)->toFormattedDateString() }}</p>
                        </div>
                        <div class="col-md-12">
                            <h4>Updated at:</h4>
                            <p>{{ \Carbon\Carbon::parse($category->updated_at)->toFormattedDateString() }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
