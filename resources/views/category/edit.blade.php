@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 mb-3">
            <h4>Form Edit Category</h4>
        </div>
        <div class="col-md-6">
            @if($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <form action="{{ route('category.update', $category->id) }}" method="post">
                {{ method_field('PUT') }}
                {{ csrf_field() }}
                <div class="form-group mb-3">
                    <label>Category Name:</label>
                    <input name="name" type="text" class="form-control" value="{{ $category->name }}" />
                </div>
                <div class="form-group">
                    <button class="btn btn-md btn-primary" type="submit">
                        Submit
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
