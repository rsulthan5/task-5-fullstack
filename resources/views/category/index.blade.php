@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-3 mb-3">
            <a href="{{ route('category.create') }}" class="btn btn-md btn-dark">Add Category</a>
        </div>
        <div class="col-md-12">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Created at</th>
                        <th>Updated at</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($categories as $item)
                    <tr>
                        <td>{{ $item->name }}</td>
                        <td>{{ \Carbon\Carbon::parse($item->created_at)->toFormattedDateString() }}</td>
                        <td>{{ \Carbon\Carbon::parse($item->updated_at)->toFormattedDateString() }}</td>
                        <td>
                            <a href="{{ route('category.show', $item->id) }}" class="btn btn-sm btn-success">Detail</a>
                            @if($item->user()->first() == auth()->user())
                            <a href="{{ route('category.edit', $item->id) }}" class="btn btn-sm btn-primary">Edit</a>
                            @endif
                            @if($item->user()->first() == auth()->user())
                            <form method="post" action="{{route('category.destroy', $item->id)}}" class="d-inline">
                                @csrf
                                @method('delete')
                                <button type="submit" onclick="alert('Are you sure?')"
                                    class="btn btn-sm btn-danger">Delete</button>
                            </form>
                            @endif
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="5">Maaf tidak ada data</td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
            {!! $categories->links() !!}
        </div>
    </div>
</div>
@endsection
