@if(session()->has('success'))
<div class="container">
    <div class="row">
        <div class="alert alert-success mt-3" role="alert">{{session()->get('success')}}</div>
    </div>
</div>
@elseif(session()->has('failed'))
<div class="container">
    <div class="row">
        <div class="alert alert-danger mt-3" role="alert">{{session()->get('failed')}}</div>
    </div>
</div>
@endif
