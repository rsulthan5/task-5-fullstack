<?php

use App\Http\Controllers\Api\Article\ArticleController;
use App\Http\Controllers\Api\Auth\PassportAuthController;
use App\Http\Controllers\Api\Category\CategoryController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1'], function () {
    // Auth
    Route::post('register', [PassportAuthController::class, 'register']);
    Route::post('login', [PassportAuthController::class, 'login']);

    // Category
    Route::get('category', [CategoryController::class, 'index'])->name('api.category.index');
    Route::get('category/{category}', [CategoryController::class, 'show']);

    // Article
    Route::get('article', [ArticleController::class, 'index']);
    Route::get('article/{article}', [ArticleController::class, 'show']);

    Route::middleware('auth:api')->group(function () {
        // Auth
        Route::post('logout', [PassportAuthController::class, 'logout']);

        // Category
        Route::post('category', [CategoryController::class, 'store'])->name('api.category.store');
        Route::put('category/{category}', [CategoryController::class, 'update'])->name('api.category.update');
        Route::delete('category/{category}', [CategoryController::class, 'destroy']);

        // Article
        Route::post('article', [ArticleController::class, 'store']);
        Route::put('article/{article}', [ArticleController::class, 'update']);
        Route::delete('article/{article}', [ArticleController::class, 'destroy']);
    });
});
