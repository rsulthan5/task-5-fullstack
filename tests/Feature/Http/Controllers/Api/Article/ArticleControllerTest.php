<?php

namespace Tests\Feature\Http\Controllers\Api\Article;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ArticleControllerTest extends TestCase
{
    use WithFaker;

    /**
     * @test
     */
    public function it_can_get_list_data()
    {
        $this->json('GET', 'api/v1/article', ['Accept' => 'application/json'])
            ->assertStatus(200);
    }
}
