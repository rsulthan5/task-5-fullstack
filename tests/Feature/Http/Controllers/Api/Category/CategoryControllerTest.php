<?php

namespace Tests\Feature\Http\Controllers\Api\Category;

use App\Models\User;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CategoryControllerTest extends TestCase
{
    use WithFaker;

    /**
     * @test
     */
    public function it_can_store_data()
    {
        $user = User::factory()->create();

        $category = [
            "name" => $this->faker->words(3, true)
        ];

        $this
            ->actingAs($user, 'api')
            ->json('POST', 'api/v1/category', $category, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJsonStructure([
                "success",
                "data" => [
                    "id",
                    "name",
                    "user_id",
                    "updated_at",
                    "created_at",
                ],
            ]);
    }

    /**
     * @test
     */
    public function it_can_get_list_data()
    {
        $this->json('GET', 'api/v1/category', ['Accept' => 'application/json'])
            ->assertStatus(200);
    }

    /**
     * @test
     */
    public function it_can_get_data()
    {
        $user = User::factory()->create();

        $category = [
            "name" => $this->faker->words(3, true)
        ];

        $category_store = $user->categories()->create($category);

        $this->json('GET', "api/v1/category/{$category_store->id}", ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJsonStructure([
                "status",
                "data" => [
                    "id",
                    "name",
                    "updated_at",
                    "created_at",
                ],
            ]);
    }

    /**
     * @test
     */
    public function it_can_update_data()
    {
        $user = User::factory()->create();

        $category = [
            "name" => $this->faker->words(3, true)
        ];

        $category_store = $user->categories()->create($category);

        $this->actingAs($user, 'api')
            ->json('PUT', "api/v1/category/{$category_store->id}", ['name' => 'category edited'], ['Accept' => 'application/json']);
    }

    /**
     * @test
     */
    public function it_can_delete_data()
    {
        $user = User::factory()->create();

        $category = [
            "name" => $this->faker->words(3, true)
        ];

        $category_store = $user->categories()->create($category);

        $response = $this->actingAs($user, 'api')
            ->call('DELETE', "api/v1/category/{$category_store->id}");

        $response->assertStatus(200);
    }
}
